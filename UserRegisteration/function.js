var listUserStore = [];
var message = "";
var regex = /\S+@\S+\.\S+/; // ---- for validation ---
var intAge;

// ----- user object -------
function User(username, name, email, password, age) {
    this.username = username;
    this.name = name;
    this.email = email;
    this.password = password;
    this.age = age;
    this.id = Date.now(); // ---- unix timestamp ------
}

//------ average age -------
function getAverage() {
    'use strict';

    var sumOfAge = 0;

    for(var i = 0; i < listUserStore.length; i++) {
        intAge = parseInt(listUserStore[i].age);
        sumOfAge += intAge;
    }
    var averageAge = sumOfAge/listUserStore.length;
    alert("Average age is " + averageAge);
} 

//----- register user ----------

function register() {
    'use strict';

    var username = document.getElementById('username').value;
    document.getElementById('username').value = " "; // ---- clear form field on submission
    
    var name = document.getElementById('name').value;
    document.getElementById('name').value = " ";

    var email = document.getElementById('email').value;
    document.getElementById('email').value = " ";

    var password = document.getElementById('password').value;
    document.getElementById('password').value = " ";

    var age = document.getElementById('age').value;
    document.getElementById('age').value = " ";
    
    //-------------- VALIDATION -----------------

    if(username === "") {
        alert("please enter username");
        
        return false;
    }    
    if(password === "") {
        alert("please enter a valid password");
        
        return false;
    }    
    if(isNaN(age) || age <= 0 || age > 100) {
        alert("please enter your age");
        
        return false;
    }      
    if(!regex.test(email)) {
        alert("email address is not valid, PLEASE ENTER A VALID ADDRESS!");
        
        return false;
    }
    for (var i = 0; i < listUserStore.length; i++) {
        if (username === listUserStore[i].username) {
            alert("Username " + listUserStore[i].username + " already exists, please choose a different username");
            return false;
        }
    }

    var output = document.getElementById('output'); //GET THE OUTPUT TO DISPLAY THE MESSAGE
    var newUser = new User(username, name, email, password, age);
    
    console.log(newUser);
    listUserStore.push(newUser); //PUSH NEW USER INTO ARRAY
    message = " ";

    for (var j = 0; j < listUserStore.length; j++) {
        
	    message += '<h2>User Added</h2> Username: ' + listUserStore[j].username + '<br>';
	    message += 'Name: ' + listUserStore[j]["name"] + '<br>';
	    message += 'Email: ' + listUserStore[j].email + '<br>';
	    message += 'Age: ' + listUserStore[j].age;
    }

    console.log(message);    
    console.log("User ID is " + newUser.id);
    output.innerHTML = message;
    console.log(listUserStore);

    return false;    
}

//------- sort users by age ------

function sortAge() {
    listUserStore.sort(function(a, b) {
        return a.age - b.age;
    });
    
    message = " ";
    
    for (var i = 0; i < listUserStore.length; i++) {       
        message += '<h2>Sorted By Age</h2>Age: ' + listUserStore[i].age + '<br>'; 
        message += 'Name: ' + listUserStore[i]["name"] + '<br>';
        message += 'Email: ' + listUserStore[i].email + '<br>';
        message += 'Username: ' + listUserStore[i].username;
    }
    console.log(message);
    output.innerHTML = message;    
    console.log(listUserStore);
}

//---- sort users by name ------

function sortName() {
    listUserStore.sort(function(a, b) {
        if(a.name < b.name){
            return -1;
        }
        if(b.name < a.name) {
            return 1;
        }
    });

    message = " ";

    for (var i = 0; i < listUserStore.length; i++) {
        message += '<h2>Sorted By Name</h2>Name: ' + listUserStore[i].name + '<br>';
        message += 'Age: ' + listUserStore[i]["age"] + '<br>';
        message += 'Email: ' + listUserStore[i].email + '<br>';
        message += 'Username: ' + listUserStore[i].username;
    }
    console.log(message);
    output.innerHTML = message;
    console.log(listUserStore);
}

function a() {
    'use strict';
    document.getElementById('theForm').onsubmit = register;
} 

window.onload = a;